import metaScraperAuthor from "metascraper-author";
import metascraperDate from "metascraper-date";
import metaScraperDescription from "metascraper-description";
import metaScraperImage from "metascraper-image";
import metaScraperLogo from "metascraper-logo";
import metaScraperTitle from "metascraper-title";
import metaScraperClearbit from "metascraper-clearbit";
import metaScraperUrl from "metascraper-url";
import metaScraperPublisher from "metascraper-publisher";
import metaScraperManifest from "metascraper-manifest";
import metaScraperAmazon from "metascraper-amazon";
import metaScraperInstagram from "metascraper-instagram";
import metaScraperSoundCloud from "metascraper-soundcloud";
import metaScraperTelegram from "metascraper-telegram";
import metaScraperUOL from "metascraper-uol";
import metaScraperSpotify from "metascraper-spotify";
import metaScraperTwitter from "metascraper-twitter";
import metaScraperYoutube from "metascraper-youtube";
import metaScraper from "metascraper";

async function getContent(url: string): Promise<string | null> {
  try {
    return await fetch(url)
      .then((res) => res.text())
      .catch(() => null);
  } catch {
    return null;
  }
}

const metascraper = metaScraper([
  metaScraperAmazon(),
  metaScraperClearbit(),
  metaScraperInstagram(),
  metaScraperSoundCloud(),
  metaScraperSoundCloud(),
  metaScraperTelegram(),
  metaScraperUOL(),
  metaScraperSpotify(),
  metaScraperTwitter(),
  metaScraperYoutube(),
  metaScraperAuthor(),
  metascraperDate(),
  metaScraperDescription(),
  metaScraperAuthor(),
  metaScraperLogo(),
  metaScraperClearbit(),
  metaScraperPublisher(),
  metaScraperTitle(),
  metaScraperUrl(),
  metaScraperImage(),
  metaScraperManifest(),
]);

export default async function metadataScraper(url: string) {
  try {
    const html = await getContent(url);
    if (!html) return null;
    const metadata = await metascraper({
      url,
      html,
    });
    return metadata;
  } catch {
    return null;
  }
}
