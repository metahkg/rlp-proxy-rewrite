FROM node:20-alpine as build

ARG env
ENV env $env

WORKDIR /app

COPY ./package.json ./yarn.lock ./tsconfig.json ./
RUN apk add python3; yarn install --frozen-lockfile --timeout 1000000; apk del python3;

COPY ./src ./src

RUN if [ "$env" = "dev" ]; then mkdir dist; else yarn build && rm tsconfig.json; fi;

FROM node:20-alpine

ARG env
ENV env $env

WORKDIR /app

COPY --from=build /app/package.json* /app/tsconfig.json* /app/yarn.lock ./

COPY --from=build /app/dist ./dist

RUN apk add python3; if [ "$env" = "dev" ]; then yarn install; else yarn install --production --frozen-lockfile --timeout 1000000; fi; yarn cache clean; apk del python3;

RUN chown -f node:node /app

USER node

CMD if [ "$env" = "dev" ]; then yarn dev; else yarn start; fi;
